The goal of this task is to create an application which shows the weather for 5 cities.
In order to do it I created a single html page. To change the page I used routing. There are two routes, first for the main page, second one for the inner one.
On the main page I showed 5 block for each of the cities with the information according to the task. As UI framework I used Twitter bootstrap.
There are two controllers, two services and one directive.
First controller is mainCtrl I used for showing main blocks via weather-widget directive where I put array cities id.
Second controller is used for rendering additional information such as sea level.
Service getDataServer is used for getting data from server.
Service convertArray is used for string creating which is used for url, which is used in getDataServer service.
Directive weatherWidget is for rendering weather info. The goal of creating this directive is reusing component in different places.